function [fig_position] = setDefaultDesktopFnc() 

%% only commandline
% desktop = com.mathworks.mde.desk.MLDesktop.getInstance;
% desktop.restoreLayout('Default');

%% set working desktop sizes
screenSize = get(0,'screensize');
screenSizeX = screenSize(1,3);
screenSizeY = screenSize(1,4);

%% j position command line
JPOS = CmdWinTool('JPosition');
% put command line into the fifth
positionX = round((screenSizeX/5)*3);
JPOS(1,1) = positionX;
JPOS(1,2) = 0;
JPOS(1,3) = screenSizeX - positionX;
JPOS(1,4) = screenSizeY;
CmdWinTool('JPosition', JPOS);

%% details
%without toolbar
CmdWinTool('lean');
CmdWinTool('top');

%% figure position
fig_position = [0 0 positionX screenSizeY];
