function [ status, R] = ifExistGetRC(pacient, D)
    index = find(cellfun(@(x)(strcmp(pacient,x)), D.corpus(:,1)));
    if isempty(index)
        status = false;
        R = [];           
    else
        status = true;        
        R = index;
    end        
end

