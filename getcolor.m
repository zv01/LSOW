function [ color ] = getcolor( pres,norm )

% hranice1 = 200;
% hranice2 = 400;
% hranice3 = 600;
% hranice4 = 800;
%             if (pres < hranice1)
%                 color = 'y';
%             elseif (pres < hranice2)
%                 color = 'c';
%             elseif (pres < hranice3)
%                 color = 'b';
%             elseif (pres < hranice4)
%                 color = 'm';
%             else
%                 color = 'k';
%             end       
            if (norm)                
                hranice1 = 0.25;
                hranice2 = 0.5;
                hranice3 = 0.75;                
            else    
                hranice1 = 250;
                hranice2 = 500;
                hranice3 = 750;
            end
            
                
                if (pres < hranice1)
                    % <25% (0 - 25%)
                    color = 'c';
                elseif (pres < hranice2)
                    % <50% (25% - 50%)
                    color = 'b';
                elseif (pres < hranice3)
                    % <75% (50% - 75%)
                    color = 'm';
                else
                    % <100% (75% - 100%)
                    color = 'k';
                end             

end

