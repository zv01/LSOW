function [ pacient_names ] = get_pacient_names( root )
currdir = pwd;
cd(root);
%% remove files (searching for dirs)
data_struct = dir;
data_struct = data_struct(logical([data_struct.isdir]));
%% remove . & ..
data_struct = data_struct(~cellfun(@(x)(strcmp(x,'.') || strcmp(x,'..')),{data_struct.name}));
%% get all pacients names
pacient_names = {data_struct.name};
cd(currdir);
end

