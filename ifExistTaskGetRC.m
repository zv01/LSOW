function [ status, C] = ifExistTaskGetRC(task, D)
    task = ['K' task '_']
    index = find(cellfun(@(x)(~isempty(strfind(x,task))), D.corpus(1,:)));
    if isempty(index)
        status = false;
        C = [];           
    else
        status = true;        
        C = index;
    end        
end

