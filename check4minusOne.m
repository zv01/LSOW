function [ status ] = check4minusOne( check_value)

    if (isnan(check_value))
        status = false;
        disp('// nan value');
        return;
    end
    
    if (isempty(check_value))
        status = false;
        disp('// empty value');
        return;
    end
    
    if (isnumeric(check_value)&&(check_value == -1))
        status = true;
        disp('// -1 value');
        return;
    end
    
    if (isnumeric(check_value))
        status = false;
        disp(['// value: ' num2str(check_value)]);
        return;
    end
end

