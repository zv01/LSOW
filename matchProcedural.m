function newcorpus = matchProcedural(corpus, CON, C, D)

%% first we test what kinda matchPartner we are dealing with

%% check for value
% 0 ... we have healthy person
% 1 ... we have deseased person
% other values we dont care about
% status = false;
DD_indexes = cell(0,0);
HC_indexes = cell(0,0);
% DD are matched in incemental order
% HC are matched in decimal order
inkr = 0;
% task_index = D.index_first_task;
task_index = C;

for i = 2:size(corpus, 1)        
    currValue = corpus{i, task_index};
    is_present = check4value( currValue, CON.dd);
    
    if is_present
        DD_indexes{size(DD_indexes,1)+1,1} = i;
        % save sex
        DD_indexes{size(DD_indexes,1),2} = corpus{i, CON.sex};
        % save class
        DD_indexes{size(DD_indexes,1),3} = corpus{i, CON.class};
    end
    
    is_present = check4value( currValue, CON.hc);    
    
    if is_present
        HC_indexes{size(HC_indexes,1)+1,1} = i; 
        % save sex
        HC_indexes{size(HC_indexes,1),2} = corpus{i, CON.sex};
        % save class
        HC_indexes{size(HC_indexes,1),3} = corpus{i, CON.class};
    end        
end

SMALLERtmp = [];
BIGGERtmp = [];

version1 = true;
if (size(DD_indexes,1) > size(HC_indexes,1))
    SMALLERtmp = HC_indexes;
    BIGGERtmp = DD_indexes;
else
    SMALLERtmp = DD_indexes;
    BIGGERtmp = HC_indexes;    
    version1 = false;
end

SMALLERtmp(:,4) = {[]};
BIGGERtmp(:,4) = {[]};
%% stepping through SMALLER group and searching for mates
for j = 1:size(SMALLERtmp,1)
    curr_class = SMALLERtmp{j,3};
    curr_sex = SMALLERtmp{j,2};
    curr_i = SMALLERtmp{j,1};
    
    [found, BIGGERtmp] = searchInCell4Value(BIGGERtmp, curr_sex, curr_class);
    if isempty(found)
        fprintf(['Nebyl nalezen vhodn� ekvivalent pro pacienta ' corpus{curr_i,1} ' \n']);
        SMALLERtmp{j,4} = [];
    else
        inkr = inkr + 1;
        SMALLERtmp{j,4} = inkr;
        BIGGERtmp{found,4} = inkr;
    end    
end        

SMALLERtmp = clean_vector( SMALLERtmp, 4 );
BIGGERtmp = clean_vector( BIGGERtmp, 4 );

%% now we are changing the actual labels for mathch persons
if version1
    %smaller = HC; bigger = DD
    HC = SMALLERtmp;
    DD = BIGGERtmp;
else
    %smaller = DD; bigger = HC
    HC = BIGGERtmp;
    DD = SMALLERtmp;    
end

%% changing values of the label
DD = addOrDevide( 'add', CON, DD );
HC = addOrDevide( 'devide', CON, HC );

%% applying new labels to the corpus
newcorpus = change_labels( corpus, C, DD );
newcorpus = change_labels( newcorpus, C, HC );

%% saving new corpus
xlsxname = [CON.match_xlsx num2str([randi([1 100],1,4)]) '.xlsx'];
xlswrite(xlsxname, newcorpus, 1);
info_cell = cell(5,1);
info_cell{1,1} = corpus{1,C};
info_cell{2,1} = size(DD,1);
info_cell{3,1} = size(HC,1);
info_cell{4,1} = 'DD';
info_cell{5,1} = 'HC';

load('tmp_match_store.mat');
tmp_match_store = [tmp_match_store info_cell];
% xlswrite(xlsxname, info_cell, 2, map(C, D.index_first_task));
xlswrite(xlsxname, tmp_match_store, 2);
save('tmp_match_store.mat', 'tmp_match_store');

fprintf(['\\ vytvo�en soubor s matchnut�mi pacienty: ' xlsxname '\n']);
fprintf(['\\ Po�et pacient� DD: ' num2str(size(DD,1)) ' \n']);
fprintf(['\\ Po�et pacient� HC: ' num2str(size(HC,1)) ' \n']);

match_check_status = checkColumnsLabels(newcorpus, corpus, C, CON);

if match_check_status
    fprintf(['\\ Cvi�en� ' info_cell{1,1} ' bylo matchnuto spr�vn�! \n']);
else
    fprintf(['\\ Cvi�en� ' info_cell{1,1} ' NEBYLO matchnuto spr�vn�! \n']);
end

split_status = splitData(newcorpus, C, CON, xlsxname);
    
end

