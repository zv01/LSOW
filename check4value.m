function [ is_present ] = check4value( value, nest)

    is_present = false;
    
    if (isnan(value))        
        disp('// nan value!!!');
        return;
    end
    
    if (isempty(value))        
        disp('// empty value!!!');
        return;
    end
   
    if (isnumeric(value) && (value == nest))
        is_present = true;
        disp(['// Values are equvivalent!!! (' num2str(value) ' == ' num2str(nest) ')']);
        return;
    end
    
    disp(['// Currently we dont care about this value!(' num2str(value) ')']);
end

