function match2str(CON)
%% vybr�n� cvi�en�
key = CON.match_xlsx;

root = dir;
names = {root.name};
indexes = find(cellfun(@(x)(~isempty(strfind(x,key))), names));
roller = 1;
p_press = false;
press_roller = 1;
list_files = false;
if isempty(indexes)
    fprintf('\\ Nebyly nalezeny ��dn� vhodn� soubory pro p�evod label� na string!!! \n');
else
    mymsg = ['>> Vyberte soubor match_file, kter� chcete upravit: \n' ...
        '>> Pohybujte se nahoru a dol� ve v�b�ru pomoc� kl�vesy 2 a 8\n' ...
        '>> Potvrzujete kl�vesou p\n' ...        
        '>> Konec v�b�ru pomoc� kl�vesy q \n' ...
        '>> Zru�it v�b�r pomoc� kl�vesy z \n' ...
        '>> Vyvol�n� n�pov�dy pomoc� kl�vesy i \n' ...
        '>> Vyvol�n� v�ech match soubor� pomoc� kl�vesy l \n' ...
        '>> Zadejte kl�vesu: \n'];
    myinput = input(mymsg, 's');
    while ~strcmp(myinput,'q')        
        switch myinput
            case '2'
                fprintf('\\ V�b�r sm�rem dol�: \n');
                if ~((roller + 1) > size(indexes,2))
                    roller = roller + 1;
                end                
            case '8'
                fprintf('\\ V�b�r sm�rem nahoru: \n');
                if roller ~= 1
                    roller = roller - 1;
                end                
            case 'p'
                fprintf('\\ Potvrzuji soubor: \n');
                press_roller = roller;
                p_press = true;
            case 'i'
                fprintf(mymsg);
            case 'l'
                list_files = true;
            case 'z'
                fprintf(['\\ Zru�en v�b�r souboru! \n']);
                p_press = false;
            otherwise
                fprintf('\\ Nezn�m� kl�vesa!\n');
        end
        
        if list_files
            fprintf(['\\ Seznam mo�n�ch match soubor�: \n']);
            cellfun(@(x,y,k)(fprintf(['\\ ' num2str(k) ': ' x ', datum: ' y '\n'])), {root(indexes).name}, {root(indexes).date}, num2cell([1:size(indexes,2)]));
            list_files = false;            
        end
        
        if p_press
            fprintf(['\\ Zvolen� soubor �. ' num2str(press_roller) ': ' root(indexes(press_roller)).name ' \n']);
        end
        fprintf(['\\ Sou�asn� soubor �. ' num2str(roller) ': ' root(indexes(roller)).name ' \n']);
        myinput = input('\\ Zadejte kl�vesu: ', 's');
    end     
     
    %% process matching
    if p_press
        % we are working with the file, even when user not
        % allowed us or not
        match_xlsx_name = root(indexes(press_roller)).name;
        [~,~,match_file] = xlsread(match_xlsx_name, 1);
        [~,~,match_help_ariables] = xlsread(match_xlsx_name, 2);
        tasks = get_all_tasks();
        first_line = match_file(1,:);
        are_present = check_all_tasks_presence( first_line, tasks );
        
        if ~are_present
            fprintf('\\ V souboru chyb� n�kter� cvi�en�!!! \n');
            return;
        end
                
        p_msg = ['\\ Zvolen� soubor �. ' num2str(press_roller) ': ' root(indexes(press_roller)).name ' \n' ...
                 '\\ bude d�le transformov�n (labely na string), chcete pokra�ovat? (a/n) \n'];
        myinput = input(p_msg, 's');
        while ~strcmp(myinput, 'n')
            if strcmp('a', myinput)                
                first_task_index = find(cellfun(@(x)(strcmp(x,tasks{1,1})),first_line));
                last_task_index = find(cellfun(@(x)(strcmp(x,tasks{1,end})),first_line));
                
                for i = first_task_index : last_task_index
                    for k = 2:size(match_file,1)
                        skip = false;
                        curr_label = match_file{k,i};
                        if isnan(curr_label) || isempty(curr_label)
                            skip = true;
                        end
                        
                        if (~skip) && (curr_label < 1) && (curr_label > 0)
                            % healthy (by this number we devide)
                            actual_value = curr_label * CON.label_healthy;
                            match_file{k,i} = [CON.hc_str num2str(actual_value)];
                        end
                        
                        if (~skip) && (curr_label > CON.label_deseased)
                            % deseased (we add this number to the label)
                            actual_value = curr_label - CON.label_deseased;
                            match_file{k,i} = [CON.dd_str num2str(actual_value)];                            
                        end
                    end
                end
                new_relabeled_xlsx_name = [match_xlsx_name(1:end-5) '_relabeled.xlsx'];
                xlswrite(new_relabeled_xlsx_name, match_file, 1);
                xlswrite(new_relabeled_xlsx_name, match_help_ariables, 2);
                fprintf(['\\ V�sledky p�elabelov�n� byly ulo�eny do souboru: ' new_relabeled_xlsx_name '\n']);
                break;
            else
                myinput = input(['Zad�na nezn�m� kl�vesa!!!\n ' p_msg], 's');
            end                            
        end
    end
end