function [ D ] = check_presence_pacients( D, CON)
pacient_names = D.pacient_names;
tasks = D.tasks;
corpus = D.corpus;

isPresent = checkTaskPresence(D, tasks{1,1});
if (isPresent)
    mode = 'NORMAL';
else
    mode = 'FRESH';
end

%% first task
first_task = tasks{1,1};

    switch mode
        case 'FRESH'
        %% INFO rmation print
        fprintf(CON.fresh_mode);
            
        %% put tasks columns at the end of the xlsx file
        corpus_row_size = size(corpus,1);
        tasks_column_size = size(tasks,2);
        new_tasks = cell(corpus_row_size, tasks_column_size);
        new_tasks(1,:) = deal(tasks(:));
        corpus = [corpus new_tasks];
        index_first_task = find(cellfun(@(x)(strcmp(x, first_task)), corpus(1,:))); 

        %% search for the presence
        numOflost = 0;
        for i = 2:size(corpus,1)
            index_pacient_name = corpus{i,1};  
        %     i
            %% first we check if we can continue
            %is empty?
            ready2go = true;
            if (isempty(index_pacient_name))
                ready2go = false;
            end

            if (ready2go && logical(sum(isnan(index_pacient_name))))
                ready2go = false;
            end

            status = 'tmp';
            if (ready2go)
                status = find(cellfun(@(x)(strcmp(x,index_pacient_name)), pacient_names));
            end
            if (isempty(status))
                disp(['//' index_pacient_name '(' num2str(i) ') was not found in the data']);
                numOflost = numOflost + 1;
                corpus(i, index_first_task:end) = deal(num2cell(CON.np));
            end
        end
        disp(['// Together there was not found ' num2str(numOflost) ' pacients in the data folder']);
        D.corpus = corpus;
        
%         case 'NORMAL'          
%             %% CHECK for presence of all tasks
%             size_of_all_tasks = size(tasks,2);            
%             if (sum(cellfun(@(x)(checkTaskPresence(D, x)),tasks)) == size_of_all_tasks)
%                 fprintf(CON.normal_mode_true);
%             else
%                 fprintf(CON.normal_mode_false);
%                 error(CON.fatal);
%             end                        
%             
%             index_first_task = find(cellfun(@(x)(strcmp(x, first_task)), corpus(1,:)));
%             %% CHECK for presence of data of pacients && CHECK if the missing pacient isnt already labeled
%             %% search for the presence
%         numOflost = 0;
%         for i = 2:size(corpus,1)
%             index_pacient_name = corpus{i,1};  
%         %     i
%             %% first we check if we can continue
%             %is empty?
%             ready2go = true;
%             if (isempty(index_pacient_name))
%                 ready2go = false;
%             end
% 
%             if (ready2go && logical(sum(isnan(index_pacient_name))))
%                 ready2go = false;
%             end
% 
%             status = 'tmp';
%             if (ready2go)
%                 status = find(cellfun(@(x)(strcmp(x,index_pacient_name)), pacient_names));
%             end
%             if (isempty(status))                
%                 %% pacient was not found
%                 numOflost = numOflost + 1;
%                 disp([index_pacient_name '(' num2str(i) ') was not found in the data']);                
%                 one_row_of_labels = corpus(i, index_first_task:end);
%                 if (~all(cellfun(@(x)(isempty(x)), one_row_of_labels)))
%                     %% but has some labeled tasks (?)
%                     disp([index_pacient_name '(' num2str(i) ') was not found in the data AND has already some labeled tasks!']);
%                     error(CON.fatal);
%                 else
%                     %% pacient was not found and has no labeled tasks
%                     corpus(i, index_first_task:end) = deal(num2cell(CON.np));
%                 end
%                                 
%             end           
%         end
%         disp(['Together there was not found ' num2str(numOflost) ' pacients in the data folder']);
%         D.corpus = corpus;
                        
    end    
index_first_task = find(cellfun(@(x)(strcmp(x, first_task)), corpus(1,:)));
D.index_first_task = index_first_task;
end

