function strokes = separate_strokes(data)

% strokes = separate_strokes(data)
% 
% This function returns a cell array with separated strokes.
% 
% data          - SVC data in the matrix
% strokes{i,1}  - SVC data of the i-th stroke
% strokes{i,2}  - pen down(1)/up(0) state

%% Calculate the stroke borders
brds = [0; find(abs(data(1:end-1,4)-data(2:end,4))); size(data,1)];

%% Extract the strokes
strokes = cell(size(brds,1)-1,2);

for i = 1:size(brds,1)-1
    strokes{i,1} = data(brds(i)+1:brds(i+1),:);
    strokes{i,2} = data(brds(i)+1,4);
end