function [found, mycell] = searchInCell4Value(mycell, value1, value2)
%% searching for desired value in the specific oolumn of the cell
% je1NAN = false;
% if (strcmp('NaN', value1) || strcmp('NAN', value1) || strcmp('nan', value1))
%     je1NAN = true;
% end
% 
% je2NAN = false;
% if (strcmp('NaN', value2) || strcmp('NAN', value2) || strcmp('nan', value2))
%     value2 = true;
% end

tmp = [];
found = [];

%% sex searching
for k = 1:size(mycell,1)
    curr_value = mycell{k, 2};    
    if isnan(curr_value) && isnan(value1) && isempty(mycell{k,4})
        tmp = k;        
    end
    
    if (value1 == curr_value) && isempty(mycell{k,4})
        tmp = k;        
    end
    
    if ~isempty(tmp)
        curr_value_2 = mycell{k, 3};
        if isnan(curr_value_2) && isnan(value2) && isempty(mycell{k,4})
            found = k;
            break;
        end

        if (value2 == curr_value_2) && isempty(mycell{k,4})
            found = k;
            break;
        end 
        tmp = [];    
    end
end
