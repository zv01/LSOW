function [ new_vector ] = clean_vector( vector, key_column )

new_vector = vector;
clean_index = cellfun(@(x)(isempty(x)),vector(:,key_column));

if ~any(clean_index)
    fprintf('// Vector didnt contained any empty values. \n');
    return;
end

new_vector = vector(~clean_index, :);
end