function [scores] = fill_x_in_scores_where_DD(scores, DD_pacients)    
    for i = 1:size(DD_pacients,1)
        DD_log_vec = cellfun(@(x)(strcmp(x,DD_pacients{i,1})), scores(1,:));
        DD_index = find(DD_log_vec);     
        if size(DD_index,2) ~= 1
            error('There are duplicate names of pacients in scores variable');
        end
        scores(2:end, DD_index) = deal({'x'});
    end
end

