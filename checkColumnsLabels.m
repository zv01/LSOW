function match_check_status = checkColumnsLabels(newcorpus, corpus, C, CON)
new_labels = newcorpus(:, C);
origin_lab = corpus(:,C);

if ~strcmp(new_labels{1,1}, origin_lab{1,1})
    error('\\ N�zvy matchnut�ch cvi�en� se nerovnaj�!!!');
end

actual_new = -999999;
for i = 2:size(origin_lab,1)
    curr_origin = origin_lab{i,1};
    curr_new_la = new_labels{i,1};
    match_check_status = false;
    
    if (curr_new_la < 1) && (curr_new_la > 0)
        actual_new = 0;
    end
    
    if (curr_new_la > CON.label_deseased)
        actual_new = 1;
    end
    
    if (actual_new == curr_origin)
        match_check_status = true;
    end
    
    if isnan(curr_origin) && isnan(curr_new_la)
        match_check_status = true;        
    end
    
    if isempty(curr_origin) && isempty(curr_new_la)
        match_check_status = true;        
    end
    
    if (curr_origin == curr_new_la)
        match_check_status = true;        
    end
    
%     i
    
    if ~match_check_status
        error(['Labely pro cvi�en� ' new_labels{1,1} 'nebyly stejn�! (origin: ' num2str(curr_origin) ', new label: ' curr_new_la ')\n']);
    end
end    
