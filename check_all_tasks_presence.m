function [ are_present ] = check_all_tasks_presence( first_line, tasks )
    are_present = true;
    for i = 1:size(tasks,2)
        task = tasks{1,i};
        if isempty(find(cellfun(@(x)(strcmp(x,task)),first_line)))
            are_present = false;
            break;
        end
    end
end

