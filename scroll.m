clc;
clear all;
close all;
%% D struct
% D is struct for storing all important data, that can be further easily
% passed through functions
D = struct();

%% Loading of all constants
CON = init_file();

%% Reading xlsx files
[~, ~, D.corpus] = xlsread(CON.corpus_file_name);

%% Creating tasks columns
D.tasks = get_all_tasks();

%% Create pacient names from directories saved in data folder
D.pacient_names = get_pacient_names(CON.data_folder);

%% Save presence of pacients into the corpus file
D = check_presence_pacients(D, CON);

%% Open xlsx file
winopen(CON.corpus_file_name);

%% function for stepping through the cells
D = startAndstep(D, CON);