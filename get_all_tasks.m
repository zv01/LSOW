function [tasks] = get_all_tasks()
tasks = struct([]);
% TSK1_spiral
tasks(size(tasks,2)+1).name = 'TSK1_spiral';
% TSK2_spiral
tasks(size(tasks,2)+1).name = 'TSK2_spiral';
% TSK3_l 
tasks(size(tasks,2)+1).name = 'TSK3_l';
% TSK4_flipped_l
tasks(size(tasks,2)+1).name = 'TSK4_flipped_l';
% TSK5_saw
tasks(size(tasks,2)+1).name = 'TSK5_saw';
% TSK6_rainbow
tasks(size(tasks,2)+1).name = 'TSK6_rainbow';
% TSK7_connected_l
tasks(size(tasks,2)+1).name = 'TSK7_connected_l';
% TSK8_recognition
tasks(size(tasks,2)+1).name = 'TSK8_recognition';
% TSK9_recall
tasks(size(tasks,2)+1).name = 'TSK9_recall';
% TSK10_recognition
tasks(size(tasks,2)+1).name = 'TSK10_recognition';
% TSK11_recall
tasks(size(tasks,2)+1).name = 'TSK11_recall';
% TSK12_recognition
tasks(size(tasks,2)+1).name = 'TSK12_recognition';
% TSK13_recall
tasks(size(tasks,2)+1).name = 'TSK13_recall';
% TSK14_recognition
tasks(size(tasks,2)+1).name = 'TSK14_recognition';
% TSK15_recall
tasks(size(tasks,2)+1).name = 'TSK15_recall';
% TSK16_recognition
tasks(size(tasks,2)+1).name = 'TSK16_recognition';
% TSK17_recall
tasks(size(tasks,2)+1).name = 'TSK17_recall';
% TSK18_recognition
tasks(size(tasks,2)+1).name = 'TSK18_recognition';
% TSK19_recognition
tasks(size(tasks,2)+1).name = 'TSK19_recognition';
% TSK20_recognition
tasks(size(tasks,2)+1).name = 'TSK20_recognition';
% TSK21_recognition
tasks(size(tasks,2)+1).name = 'TSK21_recognition';
% TSK22_reflection
tasks(size(tasks,2)+1).name = 'TSK22_reflection';
% TSK23_reflection
tasks(size(tasks,2)+1).name = 'TSK23_reflection';
% TSK24_reflection
tasks(size(tasks,2)+1).name = 'TSK24_reflection';
% TSK25_reflection
tasks(size(tasks,2)+1).name = 'TSK25_reflection';
% TSK26_reflection
tasks(size(tasks,2)+1).name = 'TSK26_reflection';
% TSK27_boat_recognition
tasks(size(tasks,2)+1).name = 'TSK27_boat_recognition';
% TSK28_boat_recall
tasks(size(tasks,2)+1).name = 'TSK28_boat_recall';
% TSK29_signature
tasks(size(tasks,2)+1).name = 'TSK29_signature';
% TSK30_zajic
tasks(size(tasks,2)+1).name = 'TSK30_zajic';
% TSK31_hana
tasks(size(tasks,2)+1).name = 'TSK31_hana';
% TSK32_radek
tasks(size(tasks,2)+1).name = 'TSK32_radek';
% TSK33_brzy
tasks(size(tasks,2)+1).name = 'TSK33_brzy';
% TSK34_eva
tasks(size(tasks,2)+1).name = 'TSK34_eva';
% TSK35_gusta
tasks(size(tasks,2)+1).name = 'TSK35_gusta';
% TSK36_zitra
tasks(size(tasks,2)+1).name = 'TSK36_zitra';
% TSK37_free
tasks(size(tasks,2)+1).name = 'TSK37_free';

%% converting to cell
tasks = {tasks.name};
end

