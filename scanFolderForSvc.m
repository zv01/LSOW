function [data, found] = scanFolderForSvc( D, CON, path2folder, task )
    actual_value = D.actual_value;    
    if (~check4minusOne(actual_value{:}))
        currdirr = pwd;
        cd(path2folder);
        scanFolder = dir;
        cd(currdirr);
        %get_splitted_svc_data_labels

        %% remove . & ..
        scanFolder = scanFolder(~cellfun(@(x)(strcmp(x,'.') || strcmp(x,'..')),{scanFolder.name}));
        possiblesvc = {scanFolder.name};
        svcPresence = cellfun(@(x)(strfind(x,'.svc')),possiblesvc, 'UniformOutput', false);
        issvcVec = cellfun(@(x)(~isempty(x)), svcPresence);
        svcFiles = possiblesvc(issvcVec);

        %% search in labfiles for task
        found = false;
        for i = 1:size(svcFiles,2)
            currSVC = svcFiles{1,i};
            pth_svc = [path2folder currSVC];
            data = get_splitted_svc_data_labels_modified(pth_svc);
            isPresent = cellfun(@(x)(strcmp(task, x)), data(:,2));
            if (any(isPresent))            
                select = logical([isPresent isPresent]);
                if (size(data,1)~= 1)
                    data = data(select).';
                end
                found = true;
                break;
            end                
        end

        if (~found)
            data = [];
        end        
    else
        data = [];
        found = false;
        disp('!!!!! nelze vykreslit - cvi�en� nen� p��tomn� !!!!');
    end
end

