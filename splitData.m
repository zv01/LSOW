function split_status = splitData(newcorpus, C, CON, xlsxname)
    split_status = [];
    source = CON.database2build;
%     goal = CON.buildedDatabase;    
    task = newcorpus{1,C};
    goal = task;
    % 0 = healthy, 1 = deseased
    myroot = pwd;
    % create feature       
    outcome = checkFile( [myroot '\' ], goal, true);
    counter_tasks = 0;
    if outcome == 0
        mkdir(goal);
        fprintf(['Lze proto vytvo�it novou slo�ku: ' goal '\n']);
    else      
        while outcome > 0 
            goal = [goal '_' num2str(randi([1 100],1,2))];
            goal= goal(find(~isspace(goal)));
            outcome = checkFile( [myroot '\' ], goal, true);
            if outcome == 0
                fprintf(['Slo�ka pro build datab�ze u� existuje, bude vytvo�en� podobn�: ' goal '\n']);
                mkdir(goal);
            end            
        end
    end
    
    %% create folders for saving
    dd_folder = [goal '\' CON.dir_dis];
    hc_folder = [goal '\' CON.dir_healthy];
    
    mkdir(dd_folder);
    mkdir(hc_folder);
        
    for i = 2:size(newcorpus,1)
        diagnosis = [];
        curr_label = newcorpus{i,C};
        if (curr_label < 1 ) && (curr_label > 0)
            % healthy
            diagnosis = 0;
        end
        
        if (curr_label > CON.label_deseased)
            % healthy
            diagnosis = 1;
        end
        
        if ~isempty(diagnosis)
            pacient = newcorpus{i,3};
            status = checkPacientPresence( source, pacient );
            if ~status
                load('tmp_match_store_2.mat');
                info_cell = cell(2,1);
                info_cell{1,1} = 'Nenalezen� pacient:';
                info_cell{2,1} = pacient;                
                tmp_match_store_2 = [tmp_match_store_2 info_cell];
                xlswrite(xlsxname, tmp_match_store_2, 3);
                save('tmp_match_store_2.mat', 'tmp_match_store_2');                
            else
                pacient_folder_path = [source '\' pacient];
                cd(pacient_folder_path);
                pacient_dir = dir;
                pacient_dir = pacient_dir(~[pacient_dir.isdir]);            
                cd(myroot);
                %create needle
                needle_index = strfind(task, '_');
                needle_index = needle_index(1,1);
                needle = [task(1:(needle_index-1)) '('];
                stack = {pacient_dir.name}.';
                search_res = cellfun(@(x)(strfind(x,needle)),stack, 'UniformOutput', false);
                search_res_index = find(cellfun(@(x)(~isempty(x)),search_res));
                if isempty(search_res_index)
                    load('tmp_match_store_3.mat');
                    info_cell = cell(5,1);
                    info_cell{1,1} = 'Nenalezen� cvi�en�:';
                    info_cell{2,1} = task;
                    info_cell{3,1} = needle;
                    tmp_match_store_3 = [tmp_match_store_3 info_cell];
                    xlswrite(xlsxname, tmp_match_store_3, 4);
                    save('tmp_match_store_3.mat', 'tmp_match_store_3');                
                else
                    found_task_name = pacient_dir(search_res_index).name;
                    counter_tasks = counter_tasks + 1;      
                    CopyMoveFile(found_task_name, logical(diagnosis), dd_folder, hc_folder, pacient_folder_path);       
                end
            end                        
        end                
    end
    
    fprintf(['\\ Po�et v�ech task: ' num2str(counter_tasks) ', polovina: ' num2str(counter_tasks/2) '\n']);

end