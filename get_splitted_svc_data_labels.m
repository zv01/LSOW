function data = get_splitted_svc_data_labels(pth_svc, pth_lab)

% data = get_splitted_svc_data_labels(pth_svc, pth_lab)
% 
% This function split the *.svc data according to *.lab file.
% 
% pth_svc   - path to the *.svc file
% pth_lab   - path to the *.lab file
% data      - output column cell separated strokes (first column corrsponds
%             to borders, second column to labels)

%% Paths and variables
if((nargin < 2) || isempty(pth_lab))
    [pathstr, name] = fileparts(pth_svc);
    pth_lab = fullfile(pathstr, [name '.lab']);
end

%% Get the borders
if(~exist(pth_svc, 'file'))
    error(['File ' pth_svc ' does not exist.'])
end
if(~exist(pth_lab, 'file'))
    error(['File ' pth_lab ' does not exist.'])
end

FID = fopen(pth_lab, 'r');
brd = textscan(FID,'%n%n%s');
fclose(FID);

if(~isempty(brd))
    labels = brd{3};
    brd = [brd{1} brd{2}];
else
    data = [];
    warning(['File ' pth_lab ' does not contain any data.']);
    return;
end

%% Read data and split them
FID = fopen(pth_svc,'r');        
Y = textscan(FID,'%n%n%n%d8%n%n%n','HeaderLines',1);        
fclose(FID);

Y = [Y{1}, Y{2}, Y{3}, cast(Y{4}, 'double'), Y{5}, Y{6}, Y{7}];

data = cell(size(brd,1),2);
for i = 1:size(brd,1)
    data{i,1} = Y(brd(i,1):brd(i,2),:);
    data{i,2} = labels{i};
end