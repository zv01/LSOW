function [ status ] = checkPacientPresence( data_pth, pacient )
    myroot = pwd;
    cd(data_pth);    
    dir_folder = dir;
    cd(myroot);
    
    names = {dir_folder.name};
    status = ~isempty(find(cellfun(@(x)(strcmp(x, pacient)), names)));
    

end

