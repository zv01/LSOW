function [ my_column_string ] = map( C, index_first_task )
    base = index_first_task;
    
    switch C
        case base
            my_column_string = 'AV';
        case (base + 1)
            my_column_string = 'AW';
        case (base + 2)
            my_column_string = 'AX';
        case (base + 3)
            my_column_string = 'AY';
        case (base + 4)
            my_column_string = 'AZ';
        case (base + 5)
            my_column_string = 'BA';
        case (base + 6)
            my_column_string = 'BB';
        case (base + 7)
            my_column_string = 'BC';
        case (base + 8)
            my_column_string = 'BD';
        case (base + 9)
            my_column_string = 'BE';
        case (base + 10)
            my_column_string = 'BF';
    end
            
end

