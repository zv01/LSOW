function [CON] = init_file()
%% init_file.m
% In this file are stored are constants of files or variables that will be
% than loaded into the program at the start
% CON ... constants

%% Corpus xlsx file
CON.corpus_file_name = get_latest_xlsx();

% class column
CON.class = 5;

% sex column
CON.sex = 6;

% dys Status column
CON.dysColumn = 7;

CON.data_folder = [pwd '\data'];

%% constants for children classification
% mirna grafomotoricka neobratnost 1/4
CON.dd1 = 1;

% grafomotoricka neobratnost 2/4
CON.dd2 = 2;

% dysgrafie 3/4
CON.dd3 = 3;

% z�va�n� dysgrafie 4/4
CON.dd4 = 4;

% healthy = 0
CON.norma = 0;

% not present -1
CON.np = -1;

% present but really bad -2
CON.bad = -2;

% bad labelled
CON.badLabel = -3;

%% Marks for children
% healthy (0)
CON.mH = 'q';

% mirna grafomotoricka neobratnost 1/4
CON.mdd1 = 'w';

% grafomotoricka neobratnost 2/4
CON.mdd2 = 'e';

% dysgrafie 3/4
CON.mdd3 = 'r';

% z�va�n� dysgrafie 4/4
CON.mdd4 = 't';

% mark for not present
CON.npM = 'f';

% mark bad -2
CON.badM = 'b';

% mark bad label -3
CON.badLabelM = 'v';

%% match persons labels
% healthy (by this number we devide)
CON.label_healthy = 100;

% deseased (we add this number to the label)
CON.label_deseased = 100;

%% Delete label
CON.del = 'd';

%% delete all current fugures
CON.delfig = 'x';

%% go to speficic person
CON.go2 = 'g';

%% go to speficic task
CON.go3 = 'c';

%% match dialog (matching healthy and diseased persons)
CON.match = 'm';

%% change labels to strings
CON.match2str = 'ms';

% match corpus file name (xlsx)
CON.match_xlsx = 'match_corpus';

% match corpus data folder with the already segmented data, from whom
% will be build up the new database
CON.database2build = '';
CON.buildedDatabase = '';
CON.dir_dis = 'diseased';
CON.dir_healthy = 'healthy';
CON.hc_str = '0_';
CON.dd_str = '1_';

%% Marks for console information
% information
CON.inf = 'p';

% quit
CON.quit = 'x';

% save to excel
CON.save = 's';

% up
CON.up = 'P�edchoz� pacient';
CON.upMark ='uparrow';
% down
CON.down = 'N�sleduj�c� pacient';
CON.downMark = 'downarrow';
%left
CON.left = 'P�edchoz� cvi�en�';
CON.leftMark = 'leftarrow';
%right
CON.right = 'N�sleduj�c� cvi�en�';
CON.rightMark = 'rightarrow';
%separation
CON.sep = '<<<<<<<<<< \n';
%air-trajectori on/off
CON.air = 'i';

%% drawing constants
% CON.figure_position = [0 0 800 600];
% CON.figure_position_line = [0 200 300 300];
CON.figure_position = setDefaultDesktopFnc();

%% initial text for console
CON.text_ini = [CON.sep ' ... Kl�vesy pro jednotliv� akce (kl�vesy je nutn� potvrdit ENTERem): \n' ...
    CON.inf ' ... Informace o ovl�d�n� \n' ...
    CON.quit ' ... Ukon�en� programu \n' ... 
    CON.save ' ... Ulo�en� programu do excel souboru \n' ...
    CON.upMark ' ... ' CON.up ' (nahoru) \n' ... 
    CON.downMark ' ... ' CON.down ' (dol�) \n' ... 
    CON.leftMark ' ... ' CON.left ' (vlevo) \n' ... 
    CON.rightMark ' ... ' CON.right ' (vpravo) \n' ... 
    CON.mH ' ... ' num2str(CON.norma) ' Zdrav� d�t� \n' ...    
    CON.mdd1 ' ... ' num2str(CON.dd1) ' M�rn� grafomotorick� neobratnost 1/4 \n' ...
    CON.mdd2 ' ... ' num2str(CON.dd2) ' Grafomotorick� neobratnost 2/4 \n' ...    
    CON.mdd3 ' ... ' num2str(CON.dd3) ' Dysgrafie 3/4 \n' ...    
    CON.mdd4 ' ... ' num2str(CON.dd4) ' Z�va�n� dysgrafie 4/4 \n' ...
    CON.del ' ... Smazat z�znam hodnocen� \n' ...
    CON.npM ' ... ' num2str(CON.np) ' Nep��tomnost cvi�en� nebo pacienta \n' ...
    CON.badM ' ... ' num2str(CON.bad) ' Velmi �patn� proveden� cvi�en� \n' ...
    CON.badLabelM ' ... ' num2str(CON.badLabel) ' �patn� nalabelovan� cvi�en� \n' ...            
    CON.air ' ... P�ep�na� zobrazen� in-air pohybu (defaultn� ano) \n' ...
    CON.go2 ' ... P�em�st�n� se na specifick�ho pacienta (nap��klad HK1007) \n' ...
    CON.go3 ' ... P�em�st�n� se na konkr�tn� cvi�en� (nap��klad 3) \n' ...       
    CON.sep];

%% INFO text about mode of the script
CON.fresh_mode = ['FRESH mode ... nebyly nalezeny ��dn� sloupce pro hodnocen� ->\n budou vytvo�en� nov� \n'];
CON.normal_mode_true = ['NORMAL mode ... byly nalezeny v�echny cvi�en� ve vstupn�m xlsx ->\n budou na�teny aktu�ln� labely \n'];
CON.normal_mode_false = ['NORMAL mode ... nebyly nalezeny v�echny cvi�en� ve vstupn�m xlsx ->\n dopl�te chyb�j�c� cvi�en� do fce get_all_tasks() \n'];

%% FATAL error
CON.fatal = 'Nelze pokra�ovat v programu -> viz v�pis v p��kazov� ��dce';
