function [y] = normalize_vector(x, method)

if ((nargin < 2) || (isempty(method)))
    method = 'L2';
end

switch method
    case 'L1'
        y = x./norm(x, 1);
    case 'L2'
        y = x./norm(x, 2);
    case 'inf'
        y = x./norm(x, inf);
    case 'z-score'
        y = zscore(x);
    otherwise
        y = x./max(abs(x));
end