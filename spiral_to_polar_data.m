function [z,r] = spiral_to_polar_data(x,y)

% [z,r] = spiral_to_polar_data(x,y)
% 
% This function calculates the radius and angle of each point of
% Archimedean spiral.
% 
% x     - input x-coordinate data
% y     - input y-coordinate data
% z     - angle
% r     - radius

%% Normalize the vectors
x = x-x(1)+1;
y = y-y(1);

%% Calculate r and theta
[z,r] = cart2pol(x,y);

sel = find(abs(z(1:end-1)-z(2:end)) > 5); % Find the steps
sel(end+1) = length(z);

% Add 2pi to each subsequent period
for i = 1:length(sel)-1
    z(sel(i)+1:sel(i+1)) =  z(sel(i)+1:sel(i+1)) + i*2*pi;
end