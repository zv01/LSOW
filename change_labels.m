function [ new_corpus ] = change_labels( corpus, C, new_labels )

    new_corpus = corpus;
    for i = 1:size(new_labels, 1)
        corp_index = new_labels{i,1};
        corp_label = new_labels{i,4};
        new_corpus{corp_index, C} = corp_label;
    end
    
end
