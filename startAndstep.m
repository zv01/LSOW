function [ D ] = startAndstep( D, CON )
pacient_names = D.pacient_names;
tasks = D.tasks;
corpus = D.corpus;

%% preparing text for the console
% myinput = input(, 's');
mystartmsg = '>>>> Zadejte kl�vesu (p pro informace):';
disp(mystartmsg);
myinput = getkey(1, 'non-ascii');

%row column
R = 2;
C = D.index_first_task;

match_dialog = false;
match2str_dialog = false;
in_air = true;
new_corpus = [];

%% create tmp match info file for later use
% we are deleting at the same time results from previous run
tmp_match_store = cell(0,0);
tmp_match_store_2 = cell(0,0);
tmp_match_store_3 = cell(0,0);
save('tmp_match_store.mat', 'tmp_match_store');
save('tmp_match_store_2.mat', 'tmp_match_store_2');
save('tmp_match_store_3.mat', 'tmp_match_store_3');

while ~strcmp(myinput,CON.quit)    
    switch myinput
        case CON.air
            if (in_air)
                fprintf('*** nevykreslovat in-air pohyb  ***\n');
                in_air = false;
            else
                fprintf('*** vykreslovat in-air pohyb  ***\n');
                in_air = true;
            end        
        case CON.inf
            fprintf(CON.text_ini);
        case CON.quit
            fprintf('*** konec programu !\n');
            break;
        case CON.upMark        
            if ((R-1) ~= 1)
                R = R-1;
                fprintf('*** nahoru!\n');
            end
        case CON.downMark        
            if ((R+1) ~= (size(corpus,1)+1))
                R = R+1;
                fprintf('*** dol�!\n');
            end
        case CON.leftMark        
            if ((C-1) ~= (D.index_first_task - 1))
                C = C-1;
                fprintf('*** vlevo!\n');
            end
        case CON.rightMark        
            if ((C+1) ~= (size(corpus,2)+1))
                C = C+1;
                fprintf('*** vpravo!\n');
            end    
        case CON.del            
            corpus{R,C} = [];
            fprintf('*** label vymaz�n!\n');
        case CON.npM
            corpus{R,C} = CON.np;
            fprintf('*** label: nep��tomen (-1)!\n');
        case CON.mdd4
            corpus{R,C} = CON.dd4;
            fprintf('*** label: z�va�n� dysgrafie 4/4\n');
        case CON.mdd3
            corpus{R,C} = CON.dd3;
            fprintf('*** label: dysgrafie 3/4\n');
        case CON.mdd2
            corpus{R,C} = CON.dd2;
            fprintf('*** label: grafomotoricka neobratnost 2/4\n');
        case CON.mdd1
            corpus{R,C} = CON.dd1;
            fprintf('*** label: mirna grafomotoricka neobratnost 1/4\n');
        case CON.mH
            corpus{R,C} = CON.norma;
            fprintf('*** label: zdrav� d�t�!\n');
        case CON.save
            xlsxname = ['corpus' num2str([randi([1 100],1,4)]) '.xlsx'];
            xlswrite(xlsxname, corpus);
            fprintf(['*** prom�nn� corpus ulo�ena do: \n' xlsxname '\n']); 
        case CON.badM
            corpus{R,C} = CON.bad;
            fprintf('*** label: naprosto �patn� cvi�en�!\n');
        case CON.delfig
            close all;
            fprintf('*** v�echny obr�zky cvi�en� zav�eny!\n');
        case CON.go2
            mypacient = input('????? Zadejte n�zev hledan�ho pacienta: \n', 's');   
            [ RCstatus, tmpR] = ifExistGetRC(mypacient, D);
            if RCstatus
                R = tmpR;
                fprintf('*** Pacient byl nalezen a kurzor p�esunut na jeho pozici. \n');
            else
                fprintf('*** Pacient nebyl nalezen! \n');
            end   
        case CON.go3
            findtask = input('????? Zadejte ��slo hledan�ho cvi�en�: \n', 's');   
            [ RCstatus, tmpC] = ifExistTaskGetRC(findtask, D);
            if RCstatus
                C = tmpC;
                fprintf('*** Cvi�en� bylo nalezeno a kurzor p�esunut na jeho pozici. \n');
            else
                fprintf('*** Cvi�en� nebylo nalezen! \n');
            end
        case CON.badLabelM
            corpus{R,C} = CON.badLabel;
                fprintf('*** label: �patn� nalabelovan� cvi�en�!\n');
        case CON.match
            % match partner
            match_dialog = true;  
        case CON.match2str
            match2str_dialog = true;
        otherwise
            fprintf('????? zad�n nezn�m� povel??? \n');
    end
    %% printing current position
    close all;
    fprintf(['/ row: ' num2str(R) ' \n']);
    fprintf(['/ column: ' num2str(C) ' \n']);
    fprintf(['/ pacient name: ' corpus{R,1} ' \n']);
    fprintf(['/ task name: ' corpus{1,C} ' \n']);    
    fprintf(['/ hodnota sloupce DYS = ' num2str(corpus{R, CON.dysColumn}) ' \n']);
    fprintf(['/ hodnota sloupce t��da = ' num2str(corpus{R, CON.class}) ' \n']);
    fprintf(['/ hodnota sloupce pohlav� = ' num2str(corpus{R, CON.sex}) ' \n']);    
    D.actual_value = corpus(R,C);
    D.actual_class = num2str(corpus{R, CON.class});
    D.corpus = corpus;
    fprintf(['/ hodnota aktu�ln�ho labelu: \n']);
%     fprintf(num2str(corpus{R,C}));
    drawmytask( D, CON, corpus{R,1}, corpus{1,C} , in_air); 
%     drawmytaskandline( D, CON, corpus{R,1}, corpus{1,C} ); 
    if (in_air)
        msg_air = 'ano';        
    else
        msg_air = 'ne';
    end
    fprintf(['/ vykreslen� in-air pohybu: ' msg_air ' \n']);
    
    
    %% possible matching dialog
    if (match_dialog)
        fprintf('*** Vstoupili jste do -match- dialogu: od t�to chv�le nem��ete labelovat! \n');
        fprintf('*** Pokud chcete znovu labelovat, mus�te spustit skript od za��tku! \n');
        if isempty(new_corpus)
            new_corpus = corpus;
        end        
        new_corpus = matchProcedural(new_corpus, CON, C, D);
        match_dialog = false;
    end    
    
    %% possible matching2str dialog
    if match2str_dialog
        fprintf('*** Vstoupili jste do -match2str- dialogu! \n');
        fprintf('*** Pokud chcete znovu labelovat, mus�te spustit skript od za��tku! \n');
        fprintf('*** Dialog slou�� k p�elabelov�n� matchnut�ch cvi�en� na string \n');
        match2str(CON);
        match2str_dialog = false;
    end
    
    
    %% going to the next user input
    disp(mystartmsg);
    myinput = getkey(1, 'non-ascii');
end

