function [xlsx_name] = get_latest_xlsx()
    xlsx_name = dir;    
    
    % clean dirs
    xlsx_name([xlsx_name.isdir]) = [];
    xlsx_name = rmfield(xlsx_name, 'bytes');
    xlsx_name = rmfield(xlsx_name, 'isdir');
%     xlsx_name = rmfield(xlsx_name, 'datenum');
    
    % leave only xlsx
    xlsx_name(cellfun(@(x)(isempty(strfind(x,'.xlsx'))), {xlsx_name.name})) = [];
    
    % leatve only corpus
    xlsx_name(cellfun(@(x)(isempty(strfind(x,'corpus'))), {xlsx_name.name})) = [];
    
    % sort
    [~,ind] = sort([xlsx_name.datenum], 'descend');    
    
    %
    xlsx_name = xlsx_name(ind);
    xlsx_name = xlsx_name(1).name;
end

