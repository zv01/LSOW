function [] = drawmytask( D, CON, pacient, task )
% function [] = drawmytask( D, CON)
%     status = true;
%     if(exist(pth_dest, 'file'))
        norm = 1;
        figure_position = CON.figure_position;
        
%         pacient = ['HK1001'];
        path2folder = [CON.data_folder '\' pacient '\'];
        [data, found] = scanFolderForSvc( D, CON, path2folder, task ); 
        if ~found
            disp('// task was not found');
        end               
        
        if(~isempty(data))                        
%             if (norm)
%                 presureD = D(:,7);
%                 presureD = normalize_vector(presureD, 'iWannaClassic');
%                 D(:,7) = deal(presureD(:));
%             end
            
            for j = 1:size(data,1) % Over all objects
                strokes = separate_strokes(data{j,1});

                h = figure;
                set(h, 'Position', figure_position);
                
                for k = 1:size(strokes,1)
                    if(strokes{k,2})
                        plot(strokes{k,1}(:,1),strokes{k,1}(:,2),'b');
                    else
                        plot(strokes{k,1}(:,1),strokes{k,1}(:,2),'r');
                    end

                    hold on;

                    if(k == 1)
                        plot(strokes{k,1}(1,1),strokes{k,1}(1,2), 'go', 'MarkerSize', 8, 'LineWidth', 2, 'MarkerFaceColor', 'g');
                    end
                    if(k == size(strokes,1))
                        plot(strokes{k,1}(end,1),strokes{k,1}(end,2), 'mo', 'MarkerSize', 8, 'LineWidth', 2, 'MarkerFaceColor', 'm');
                    end
                end

                hold off;
                grid on;
                axis tight;
                myTitle = strrep([data{j,2} ' (segmented)'],'_','\_'); 
                title([myTitle ' clinical state = ' D.actual_value ' school class = ' D.actual_class]);
            end
            
            set(h, 'name', [pacient ' : ' task]);
        end
%     else
%         status = false;
%     end

end

