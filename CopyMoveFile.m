function CopyMoveFile(found_task_name, diagnosis, dd_folder, hc_folder, pacient_folder_path)


if diagnosis
    copyfile([pacient_folder_path '\' found_task_name], dd_folder);    
    mymsg = ['\\ File ' found_task_name ' copied from ' strrep(pacient_folder_path, '\', '\\') ' into ' strrep(dd_folder, '\', '\\') '\n'];    
else
    copyfile([pacient_folder_path '\' found_task_name], hc_folder);    
    mymsg = ['\\ File ' found_task_name ' copied from ' strrep(pacient_folder_path, '\', '\\') ' into ' strrep(hc_folder, '\', '\\') '\n'];    
end
fprintf(mymsg);