function [ outcome ] = checkFile( path, name, searchDir)
currdir = pwd;
cd(path);
dirfold = dir;
cd(currdir);

if searchDir
    dirfold(~[dirfold.isdir]) = [];
end
    outcome = sum(cellfun(@(x)(strcmp(name,x)), {dirfold.name}));
    
    switch outcome
        case 0
            fprintf(['\\ Nebyl nalezen po�adovan� soubor: ' name '\n']);
        case 1
            fprintf(['\\ Byl nalezen po�adovan� soubor: ' name '\n']);
        otherwise
            fprintf(['\\ Pozor!! Vyskytlo se v�ce kopii dan�ho souboru!!! (' name ')\n']);
    end
end

