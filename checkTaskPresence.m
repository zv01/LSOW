function [ isPresent ] = checkTaskPresence(D, task)
corpus = D.corpus;
firstLine = corpus(1,:);

isPresent = true;
if (isempty(find(cellfun(@(x)(strcmp(task, x)),firstLine))))
    isPresent = ~isPresent;
end

end

